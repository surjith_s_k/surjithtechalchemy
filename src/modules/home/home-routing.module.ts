import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { paths } from 'src/app/models/RouterModels/path.model';
import { HomeComponent } from './components/home/home.component';

const routes: Routes = [
  {
    path: paths.current,
    component: HomeComponent,
    data: {
      title: 'All Restaurant | Surjith TechAlchemy',
      keywords: "Surjith TechAlchemy",
    description:"Solution to the assignment provided by the company - TechAlchemy. This solution app is completely built by following the instructions and also qualifies all the test cases provided.",
    image:"https://sulonyafiles.s3.ap-south-1.amazonaws.com/WP1G90051FFV03RVBESDNPIHIZKLAXZ7.jpeg",
    url:"after deployment"
  } 
},
  {
    path: paths.restaurantDetail,
    component: HomeComponent,
    data: {
      title: 'Restaurant Details | Surjith TechAlchemy',
      keywords: "Surjith TechAlchemy",
    description:"Solution to the assignment provided by the company - TechAlchemy. This solution app is completely built by following the instructions and also qualifies all the test cases provided.",
    image:"https://sulonyafiles.s3.ap-south-1.amazonaws.com/WP1G90051FFV03RVBESDNPIHIZKLAXZ7.jpeg",
    url:"after deployment"
  } 
}
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { restaurantDetails } from 'src/app/models/ResponseModels/restaurantDetails.model';
import { ApiService } from 'src/app/services/apiCalls/api.service';
import { SnackbarService } from 'src/app/services/snackbar/snackbar.service';

@Component({
  selector: 'sur-ta-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  showDescription: boolean = false;
  details?: restaurantDetails;
  cuisines: string[] = [];

  /**
   * Creates an instance of home component.
   * @param _snackBar 
   * @param route 
   * @param apiService 
   */
  constructor(
    private _snackBar: SnackbarService,
    private route: ActivatedRoute,
    private apiService: ApiService
  ) { 
   this.showDescription = this.route.snapshot.params.id ? true : false
  }
  /**
   * on init welcomes the user for Surjith assignment and fetches the details of the restaurants
   */
  ngOnInit(): void {
    
    this._snackBar.openSnackBar('Welcome to Surjith-TechAlchemy Assignment!', 'close', 'success', 4);
    if(this.route.snapshot.params.id){
      this.showDescription = true;
      this.getDetails();
    }
    else{
      this.showDescription = false;
    }
  }

  /**
   * Gets details of restaurants
   */
  getDetails(){
     this.apiService.getRestaurantsDetails(this.route.snapshot.params.id).subscribe((res: restaurantDetails)=>{
      this.details = res;
      console.log(this.details)
     });
  }


}

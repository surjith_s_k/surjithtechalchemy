import { Component, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { restaurantDetails } from 'src/app/models/ResponseModels/restaurantDetails.model';
import { ApiService } from 'src/app/services/apiCalls/api.service';

@Component({
  selector: 'sur-ta-description',
  templateUrl: './description.component.html',
  styleUrls: ['./description.component.css']
})
export class DescriptionComponent  {
  /**
   * Input show, restaurnatData of description component
   */
  @Input() show: boolean = false;
  @Input() restaurantData?: restaurantDetails;
  constructor() {
   
   }  




  ngOnChanges(): void{
    this.show = this.show;
  }
  /**
   * Splits item hours
   * @param [item] 
   * @returns  
   */
  splitItemHours(item?:string){
    return item?.split(',');
  }

  
  
}

import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { allRestaurants } from 'src/app/models/ResponseModels/allResturant.model';
import { menuData } from 'src/app/models/ResponseModels/menu.model';
import { restaurantDetails } from 'src/app/models/ResponseModels/restaurantDetails.model';
import { ApiService } from 'src/app/services/apiCalls/api.service';



@Component({
  selector: 'sur-ta-grid-card',
  templateUrl: './grid-card.component.html',
  styleUrls: ['./grid-card.component.css']
})
export class GridCardComponent implements OnInit {

  menus: string[] = ['All'];
  menuData: menuData[] = [];
  menusSelected: Array<{}> = [];
  currentRestaurantId: number = 0;
  allCategories: string[] = [];
  allRestaurants: allRestaurants[] = [];
  allCuisines: string[] = [];
  restaurantData?: restaurantDetails;
  @Input() set restaurantsData(restaurantDetails: restaurantDetails | undefined) {
    this.restaurantData = restaurantDetails;
    if (this.restaurantData) {
      this.getMenus();
    }
  };

  @Output() cuisineEmitter = new EventEmitter();
  /**
   * Creates an instance of grid card component.
   * @param apiService 
   * @param loader 
   * @param route 
   */
  constructor(
    private apiService: ApiService,
    private loader: NgxUiLoaderService,

    private route: ActivatedRoute) {
    this.currentRestaurantId = this.route.snapshot.params?.id
  }

  /**
   * on init get restaurants list
   */
  ngOnInit(): void {
    this.loader.start();
    this.menusSelected.push(this.menus[0]);
    if (!this.currentRestaurantId) {
      this.getRestaurants();
    }
  }

  /**
   * Gets restaurants
   */
  getRestaurants() {
    this.apiService.getRestaurants().subscribe((restaurants: allRestaurants[]) => {
      this.allRestaurants = restaurants;
      restaurants.forEach(element => {
        this.allCategories = [...this.allCategories, ...element.restaurantCategory];
        this.allCuisines = [...this.allCuisines, ...element.restaurantCuisine];
      });
      this.allCategories = Array.from(new Set(this.allCategories));
      this.allCuisines = Array.from(new Set(this.allCuisines));
      this.cuisineEmitter.emit(this.allCuisines);
      this.loader.stop();
    })
  }

  /**
   * Gets router link for particular restaurant
   * @param id 
   * @returns  
   */
  getRouterLinkForItem(id: number) {
    return this.currentRestaurantId ? this.route.pathFromRoot : 'restaurant/' + id;
  }
  /**
   * Gets menus
   */
  getMenus() {
    console.log(this.restaurantData);
    if (this.restaurantData) {
      this.apiService.getMenus(this.currentRestaurantId).subscribe((res: menuData[]) => {
        this.loader.stop();
        this.menuData = res;
        this.getMenusCount();
      })
    }
  }
  /**
   * Gets menus count
   */
  getMenusCount() {
    let freqMap = new Map();
    this.menuData.forEach(element => {
      for (let i = 0; i < element.itemCategory.length; i++) {
        if (freqMap.has(element.itemCategory[i])) {
          freqMap.set(element.itemCategory[i], freqMap.get(element.itemCategory[i]) + 1);
        }
        else {
          freqMap.set(element.itemCategory[i], 1);
        }
      }
    });
    for (let [key, value] of freqMap.entries()) {
      this.menus.push(key + " (" + value + ")");
    }
  }
  /**
   * Gets image for the category button
   * @param item 
   * @returns  
   */
  getImage(item: string) {
    let imageValue = 'salad';
    switch (item.toLocaleLowerCase()) {
      case 'hot dish':
        imageValue = 'hot_dish';
        break;
      case 'baked':
        imageValue = 'Baked';
        break;
      case 'sweet':
        imageValue = 'sweet';
        break;

      default:
        break;
    }
    return imageValue;
  }
  /**
   * Determines whether the button is selected 
   * @param item 
   * @returns  
   */
  isSelected(item: string) {
    return this.menusSelected.indexOf(item) !== -1;
  }
  /**
   * Adds filters and changes color
   * @param item 
   */
  addFilters(item: string) {
    if (this.menusSelected.indexOf(item) !== -1) {
      this.menusSelected.splice(this.menusSelected.indexOf(item), 1);
    }
    else {
      this.menusSelected.push(item);
    }
  }
  
}

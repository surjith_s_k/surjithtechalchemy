import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './components/home/home.component';
import { BasicModule } from '../basic/basic.module';
import { MaterialModule } from '../material/material.module';
import { SnackbarService } from 'src/app/services/snackbar/snackbar.service';
import { GridCardComponent } from './components/grid-card/grid-card.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { DescriptionComponent } from './components/description/description.component';
import { ApiService } from 'src/app/services/apiCalls/api.service';
import { NgxUiLoaderConfig, NgxUiLoaderModule } from 'ngx-ui-loader';

const  config : NgxUiLoaderConfig = {
  "bgsColor": "#503e9d",
  "bgsOpacity": 0.5,
  "bgsPosition": "bottom-right",
  "bgsSize": 60,
  "bgsType": "ball-scale-multiple",
  "blur": 5,
  "delay": 0,
  "fastFadeOut": true,
  "fgsColor": "#fb6d3a",
  "fgsPosition": "center-center",
  "fgsSize": 100,
  "fgsType": "square-jelly-box",
  "gap": 24,
  "logoPosition": "center-center",
  "logoSize": 100,
  "logoUrl": "https://sulonyafiles.s3.ap-south-1.amazonaws.com/WP1G90051FFV03RVBESDNPIHIZKLAXZ7.jpeg",
  "masterLoaderId": "master",
  "overlayBorderRadius": "0",
  "overlayColor": "rgba(251,109,58,0.1)",
  "pbColor": "#fb6d3a",
  "pbDirection": "ltr",
  "pbThickness": 3,
  "hasProgressBar": true,
  "text": "Fetching Hotel Data",
  "textColor": "#FFFFFF",
  "textPosition": "center-center",
  "maxTime": -1,
  "minTime": 300
}
@NgModule({
  declarations: [HomeComponent, GridCardComponent, DescriptionComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    MaterialModule,
    FlexLayoutModule,
    BasicModule,
    NgxUiLoaderModule.forRoot(config),
  ],
  providers: [SnackbarService, ApiService]
})
export class HomeModule { }

import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FilterService } from '../../services/filter/filter.service';

@Component({
  selector: 'sur-ta-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  /**
   * Output toggle of header component to toggle left side bar
   */
  @Output() toggle = new EventEmitter();
  /**
   * Input  of header component to hide / show the header elements
   */
  @Input() show: boolean =true;


  open: boolean = true;
  openFilter: boolean = false;
  /**
   * Creates an instance of header component.
   * @param filterService 
   */
  constructor(
    private filterService: FilterService
    ) { }

  /**
   * on init toggle side bar
   */
  ngOnInit(): void {
    this.toggleSidenav(this.open);
  }

  /**
   * Toggles sidenav
   * @param isOpen 
   */
  toggleSidenav(isOpen:boolean){
    this.open = isOpen;
    this.toggle.emit(isOpen);
  }

  /**
   * Toggles filter
   */
  toggleFilter(){
    this.filterService.toggleFilterVisibility();
  }

}

import { Component, Input } from '@angular/core';

@Component({
  selector: 'sur-ta-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent {

  /**
   * Input cuisines, show of layout component 
   */
  @Input() cuisines: Array<string> =[];
  @Input() show: boolean = true;
  constructor() { }


}

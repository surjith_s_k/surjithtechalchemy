import { Component, ElementRef, Input, OnInit, QueryList, Renderer2, ViewChildren } from '@angular/core';
import { Subscription } from 'rxjs';
import { FilterService } from '../../services/filter/filter.service';

@Component({
  selector: 'sur-ta-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  /**
   * Input open navbar, cuisines, show  of sidebar component
   */
  @Input() open: boolean = true;
  @Input() cuisines : string[] = [];
  @Input() show: boolean = true;
  /**
   * View children cards list of sidebar component
   */
  @ViewChildren('cards', { read: ElementRef }) cards!: QueryList<ElementRef>;
  openFilter: boolean = false;
  filterSubscription: Subscription;
  filtersCuisine :Array<string> = [];
  panelOpenState = false;
  //asumned for animation purpose
  allCards = ['one', 'two', 'three', 'four'];

  /**
   * Creates an instance of sidebar component.
   * @param filterService 
   * @param renderer 
   */
  constructor(
    private filterService: FilterService,
    private renderer: Renderer2
    ) {
    this.filterSubscription = this.filterService.onUpdate().subscribe((isOpen: boolean) => {
     this.openFilter = isOpen;
  });
   }

  /**
   * on init call service of filter bar
   */
  ngOnInit(): void {
    this.openFilter = this.filterService.isFilterVisible;
    
  }

  /**
   * after view init make animations enabled for the cards
   */
  ngAfterViewInit(): void{
    for(let i = 0; i<this.cards.length; i++){
      let el = this.cards.find((item,index) => index === i)
      this.renderer.setStyle(el?.nativeElement,'top','0');
      console.log(el?.nativeElement);
      if(i){
        let prevEl = this.cards.find((item,index) => index === (i-1))
        this.renderer.setStyle(prevEl?.nativeElement,'top','15px');
        this.renderer.setStyle(prevEl?.nativeElement,'width','74%');
        this.renderer.setStyle(prevEl?.nativeElement,'left','13%');
      }
    if(i-1 > 1){
        let latterEl = this.cards.find((item,index) => index === (i-2))
        this.renderer.setStyle(latterEl?.nativeElement,'top','30px');
        this.renderer.setStyle(latterEl?.nativeElement,'width','68%');
        this.renderer.setStyle(latterEl?.nativeElement,'left','16%');
       
      }
      if(i === this.cards.length - 1){
        this.renderer.removeClass(el?.nativeElement,'hide-button');
      }
     
    }
    
  
  }
 
  /**
   * Toggles left side bar
   * @param isOpen 
   */
  toggleLeftSideBar(isOpen:boolean){
    this.open = isOpen;
  }
  /**
   * Togglesfilter side bar
   */
  toggleSideBar(){
    this.filterService.toggleFilterVisibility();
  }

  /**
   * Adds filters
   * @param item 
   */
  addFilters(item: string){
    if(this.filtersCuisine.indexOf(item) !== -1){
      this.filtersCuisine.splice(this.filtersCuisine.indexOf(item),1);
    }
    else{
      this.filtersCuisine.push(item);

    }
  }
  /**
   * Determines whether selected is
   * @param item 
   * @returns  
   */
  isSelected(item: string){
    
    return this.filtersCuisine.indexOf(item) !== -1;
  }

  /**
   * Closes card
   * @param index 
   * @returns  
   */
  closeCard(index:number){
    const allCards = this.cards.toArray();
    const currentCard = allCards[index];
    const previousCard = allCards[index - 1];
    const previousCardPrevSibling = index - 2 >= 0 ? allCards[index-2]:null;
    const previousLastCard = index - 3 >= 0 ? allCards[index-3]:null;
    this.renderer.setStyle(currentCard?.nativeElement,'top','999px');
    if(previousCard){
    
        this.renderer.setStyle(previousCard?.nativeElement,'width','80%');
        this.renderer.setStyle(previousCard?.nativeElement,'left','10%');

        this.renderer.setStyle(previousCard?.nativeElement,'top','0');
        this.renderer.removeClass(previousCard?.nativeElement,'hide-button');
    }

    if(previousCardPrevSibling){
      this.renderer.setStyle(previousCardPrevSibling?.nativeElement,'width','74%');
      this.renderer.setStyle(previousCardPrevSibling?.nativeElement,'left','13%');

      this.renderer.setStyle(previousCardPrevSibling?.nativeElement,'top','15px');
     
    }
    if(previousLastCard){
      this.renderer.setStyle(previousLastCard?.nativeElement,'width','68%');
      this.renderer.setStyle(previousLastCard?.nativeElement,'left','16%');

      this.renderer.setStyle(previousLastCard?.nativeElement,'top','30px');
       
    }
    else{return;}
  }

}

import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FilterService {
  /**
   * Determines whether filter visible is
   */
  isFilterVisible: boolean = false;

  /**
   * Filter visibility change subject of filter service
   */
  filterVisibilityChange: Subject<boolean> = new Subject<boolean>();

  /**
   * Creates an instance of filter service.
   */
  constructor()  {
      this.filterVisibilityChange.subscribe((value) => {
          this.isFilterVisible = value
      });
  }

  /**
   * Toggles filter visibility
   */
  toggleFilterVisibility() {
      this.filterVisibilityChange.next(!this.isFilterVisible);
  }
/**
 * Determines whether update on
 * @returns update 
 */
onUpdate(): Observable<any> {
    return this.filterVisibilityChange.asObservable();
}
}

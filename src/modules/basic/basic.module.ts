import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BasicRoutingModule } from './basic-routing.module';
import { LayoutComponent } from './components/layout/layout.component';
import { MaterialModule } from '../material/material.module';
import { SnackbarService } from 'src/app/services/snackbar/snackbar.service';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HeaderComponent } from './components/header/header.component';
import { FilterService } from './services/filter/filter.service';
import {SwipeCardLibModule} from 'ng-swipe-card';


@NgModule({
  declarations: [LayoutComponent, SidebarComponent, HeaderComponent],
  imports: [
    CommonModule,
    MaterialModule,
    BasicRoutingModule,
    FlexLayoutModule,
    SwipeCardLibModule

  ],
  providers:[SnackbarService, FilterService],
  exports: [LayoutComponent  ]
})
export class BasicModule { }

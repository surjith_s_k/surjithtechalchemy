import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class SnackbarService {
  /**
   * 
   * @param _snackBar 
   */
  constructor(private _snackBar: MatSnackBar){
  }
  /**
   * initiates a snackbar service for period of time mentioned and respective message is shown
   * @param message 
   * @param action 
   * @param panelClass 
   * @param time 
   */
  public openSnackBar(message: string, action: string, panelClass:string,time:number) {
      this._snackBar.open(message, action, {
        duration: time*1000,
        panelClass: [panelClass]
      });
  }
}

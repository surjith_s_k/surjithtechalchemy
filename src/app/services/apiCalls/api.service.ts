import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { globalurl, urls } from 'src/app/global/api.url';
import { allRestaurants } from 'src/app/models/ResponseModels/allResturant.model';
import { menuData } from 'src/app/models/ResponseModels/menu.model';
import { restaurantDetails } from 'src/app/models/ResponseModels/restaurantDetails.model';
import { SnackbarService } from '../snackbar/snackbar.service';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  api: string = globalurl.url;
  restaurantData?:restaurantDetails;
  /**
   * 
   * @param httpClient 
   * @param _snackBarService 
   * @param loader 
   */
  constructor(private httpClient: HttpClient, private _snackBarService: SnackbarService, private loader: NgxUiLoaderService) { }

  /**
   * to get all restaurants data
   * @returns allRestaurants[]
   */
  public getRestaurants(): Observable<allRestaurants[]> {
    
    return this.httpClient.get<{ 'allRestaurants': allRestaurants[] }>
      (this.api + urls.allRestaurants)
      .pipe(
        map(
          item => item.allRestaurants.map(item => {
            return <allRestaurants>{
              restaurantName: item.restaurantName,
              restaurantCategory: JSON.parse(item.restaurantCategory.toString()),
              restaurantCuisine: JSON.parse(item.restaurantCuisine.toString()),
              restaurantDescription: item.restaurantDescription,
              restaurantImage: item.restaurantImage,
              isOpen: item.isOpen,
              id: item.id
            }
          })
        ),
        catchError(
          this.handleError
        )
      );
  }
  /**
   * get restaurant of particualr id
   * @param id 
   * @returns  restaurantDetails
   */
  public getRestaurantsDetails(id: number): Observable<restaurantDetails> {
    return this.httpClient.get<{ 'restaurantDetail': restaurantDetails }>
      (this.api + urls.restaurantDetails+'/'+id, {
        headers:{"Authorization":"Bearer 34303304-5475-4d63-9352-0d24ed631b37"}
      })
      .pipe(
      map(item => item.restaurantDetail),
        catchError(
          this.handleError
        )
      );
  }

  /**
   * sheety did not support the menu api thus this api is commented
   * @param name 
   * @returns menuData[]
   */
  // public getRestaurantMenus(name: string): Observable<menuData[]> {
  //   return this.httpClient.get<menuData[]>
  //     (this.api + urls.menu+'/'+name)
  //     .pipe(
  //       map(
  //         item => item
  //       ),
  //       catchError(
  //         this.handleError
  //       )
  //     );
  // }

  /**
   * sheety responded with 404 error for the given api so this api is custom made assuming the response values 
   * @param id 
   * @returns menuData[]
   */
  public getMenus(id: number):Observable<menuData[]>{
    return  this.httpClient.get<menuData[][]>(urls.menuFromAsset).pipe(map(item => item[id]));
  }

  private handleError(error: HttpErrorResponse) {
    // Handle the HTTP error here
    this._snackBarService.openSnackBar('Something went wrong', 'close', 'error', 4);
    return throwError('Something wrong happened');
  }
}

import { Component } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { ChildActivationEnd, Data, Router } from '@angular/router';
import { SnackbarService } from './services/snackbar/snackbar.service';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'sur-ta-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Surjith Assignment - TechAlchemy';

  /**
   * based on the data provided in router module, the meta tags are updated.
   * @param router 
   * @param metaService 
   * @param titleService 
   * @param _snackBar
   */
  constructor(public router: Router,
    private metaService: Meta,
    private titleService: Title

  ) {

    this.router.events
      .pipe(filter(event => event instanceof ChildActivationEnd))
      .subscribe(event => {

        let snapshot = (event as ChildActivationEnd).snapshot;
        while (snapshot.firstChild !== null) {
          snapshot = snapshot.firstChild;
        }
        this.titleService.setTitle(snapshot.data.title || this.title);

        this.setMetaData(snapshot.data)

      });

  }


  /**
  * setMetaData
  */
  setMetaData(data: Data) {
    this.metaService.updateTag({ 'name': 'keywords', 'content': data.keywords }, 'name="keywords"');
    this.metaService.updateTag({ name: 'description', 'content': data.description }, 'name="description"');
    // this.metaService.updateTag({ 'name': 'twitter:card', 'content': 'summary_large_image' });
    this.metaService.updateTag({ 'name': 'twitter:title', 'content': data.title }, 'name="twitter:title"');
    this.metaService.updateTag({ 'name': 'twitter:text:title', 'content': data.title }, 'name="twitter:text:title"');
    this.metaService.updateTag({ 'name': 'twitter:description', 'content': data.description }, 'name="twitter:description"');
    this.metaService.updateTag({ 'name': 'twitter:image', 'content': data.image }, 'name="twitter:image"');
    this.metaService.updateTag({ 'name': 'twitter:image:alt', 'content': data.title }, 'name="twitter:image:alt"');
    this.metaService.updateTag({ 'name': 'twitter:url', 'content': data.url }, 'name="twitter:url"');

    this.metaService.updateTag({ 'property': 'og:title', 'content': data.title }, 'property="og:title"');
    this.metaService.updateTag({ 'property': 'og:url', 'content': data.url }, 'property="og:url"');
    this.metaService.updateTag({ 'property': 'og:image', 'content': data.image }, 'property="og:image"');
    this.metaService.updateTag({ 'property': 'og:image:alt', 'content': data.title }, 'property="og:image:alt"');
    this.metaService.updateTag({ 'property': 'og:description', 'content': data.description }, 'property="og:description"');

  }

}



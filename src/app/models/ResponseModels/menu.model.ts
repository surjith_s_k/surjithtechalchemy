export interface menuData{
    
    itemCategory: Array<string>;
    itemName: string;
    itemPhoto: string;
    itemCost: string;
} 
export interface menuResponseData{
    resName : Array<menuData>;
}
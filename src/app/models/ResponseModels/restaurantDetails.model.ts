export interface restaurantDetails {
    "restaurantName": string,
    "restaurantDescription": string,
    "openingHours":  string,
    "restaurantImage": string,
    "contactNumber": string,
    "websiteUrl": string,
    "id": number  
}
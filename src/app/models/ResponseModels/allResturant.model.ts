export interface allRestaurants{

        "restaurantName": string,
        "restaurantDescription": string,
        "restaurantCategory": Array<string>,
        "restaurantCuisine":  Array<string>,
        "restaurantImage": string,
        "isOpen": boolean,
        "id": number

  
}
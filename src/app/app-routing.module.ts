import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { paths } from './models/RouterModels/path.model';

const routes: Routes = [
  {
    path: paths.home,
    loadChildren: () => import('../modules/home/home.module').then(m=>m.HomeModule)
  },
  {
    path: paths.current,
    redirectTo: paths.home,
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

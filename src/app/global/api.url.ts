export const globalurl = {
   url: "https://api.sheety.co/bdcbafbc1f4197dda178b9e69f6ccee9/techAlchemyWebTest1/"
}

export const urls = {
    allRestaurants: "allRestaurants",
    restaurantDetails: "restaurantDetails",
    menu: "menu",
    menuFromAsset: "assets/data/menu.json"
}
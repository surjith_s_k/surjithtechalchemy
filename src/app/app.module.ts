import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {FlexLayoutModule} from '@angular/flex-layout';
import { HttpClientModule } from '@angular/common/http';
import { NgxUiLoaderConfig, NgxUiLoaderModule } from 'ngx-ui-loader';

const  config : NgxUiLoaderConfig = {
  "bgsColor": "#503e9d",
  "bgsOpacity": 0.5,
  "bgsPosition": "bottom-right",
  "bgsSize": 60,
  "bgsType": "ball-scale-multiple",
  "blur": 5,
  "delay": 0,
  "fastFadeOut": true,
  "fgsColor": "#fb6d3a",
  "fgsPosition": "center-center",
  "fgsSize": 100,
  "fgsType": "square-jelly-box",
  "gap": 24,
  "logoPosition": "center-center",
  "logoSize": 100,
  "logoUrl": "https://sulonyafiles.s3.ap-south-1.amazonaws.com/1623251518207.jpg",
  "masterLoaderId": "master",
  "overlayBorderRadius": "0",
  "overlayColor": "rgba(251,109,58,0.1)",
  "pbColor": "#fb6d3a",
  "pbDirection": "ltr",
  "pbThickness": 3,
  "hasProgressBar": true,
  "text": "Fetching Hotel Data",
  "textColor": "#000",
  "textPosition": "center-center",
  "maxTime": -1,
  "minTime": 300
}
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    NgxUiLoaderModule.forRoot(config),
    FlexLayoutModule.withConfig({ ssrObserveBreakpoints: ['xs','sm'] })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
